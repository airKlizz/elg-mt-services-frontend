module.exports = {
  i18n: {
    locales: ["en", "ru", "pl", "uk"],
    defaultLocale: "en",
  },
  experimental: {
    outputStandalone: true,
  },
};
