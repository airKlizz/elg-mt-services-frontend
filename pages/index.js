import {
  Button,
  Card,
  Container,
  Loading,
  Modal,
  Row,
  Spacer,
  Switch,
  Text,
  Textarea,
  Tooltip,
  styled,
  useClipboard,
  useInput,
  useModal,
  useTheme,
} from "@nextui-org/react";
import {
  DataTransferBoth,
  HalfMoon,
  InfoEmpty,
  Language,
  SunLight,
  Svg3DSelectFace,
} from "iconoir-react";
import React, {useState, useEffect} from "react";

import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import {useTheme as useNextTheme} from "next-themes";
import {useRouter} from "next/router";

const contents = {
  en: {
    clear: "Clear",
    copy: "Copy",
    translate: "Translate",
    translation: "Translation",
    enter: "Enter the text to translate.",
    selectLang: "Select the language for the page",
    selectSrc: "Select the source language",
    selectTgt: "Select the target language",
    selectModel: "Select the model to use",
    currentModelInfo: "The European Language Grid counts various translation models for the same languages. You can choose between all the models available.",
    currentModel: "Current model: ",
    funding:
      "The European Language Grid has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement № 825627 (ELG)",
    information: "Information",
    informationTexts: {
      p1: "This is the open translator of the European Language Grid (ELG), powered by machine translation services from the ELG catalogue, made available by the ELG community. ",
      p2: "The European Language Grid is the platform for Language Technologies (LT) in Europe, giving access to thousands of commercial and non-commercial LT services and resources in all European languages.",
      p2a: "Feel free to visit our website.",
      p3: "The creation of this translation system has been motivated by the recent invasion of Ukraine by the Russian Federation. As a European project, ELG stands with Ukraine and wants to help all Ukrainians arriving in Europe as well as the people welcoming them in their communication.",
      p4: "Please let us know if you have questions or encounter issues with the service via contact@european-language-grid.eu.",
    },
    languages: {
      English: "English",
      Ukrainian: "Ukrainian",
      Russian: "Russian",
      French: "French",
      Polish: "Polish",
    },
  },
  fr: {
    clear: "Supprimer",
    copy: "Copier",
    translate: "Traduire",
    translation: "Traduction",
    enter: "Entrez le texte à traduire.",
    selectLang: "Sélectionnez la langue de la page",
    selectSrc: "Sélectionnez la langue source",
    selectTgt: "Sélectionnez la langue cible",
    selectModel: "Sélectionnez le modèle à utiliser",
    currentModelInfo: "L'European Language Grid compte différents modèles de traduction pour les mêmes langues. Vous pouvez choisir parmi tous les modèles disponibles.",
    currentModel: "Modèle actuel: ",
    funding:
      "L'European Language Grid a reçu un financement du programme de recherche et d'innovation Horizon 2020 de l'Union européenne dans le cadre de la convention de subvention № 825627 (ELG)",
    information: "Information",
    informationTexts: {
      p1: "Il s'agit du traducteur ouvert de European Language Grid (ELG), utilisant les services de traduction automatique du catalogue ELG, mis à disposition par la communauté ELG.",
      p2: "L'European Language Grid est la plate-forme des technologies linguistiques (LT) en Europe, donnant accès à des milliers de services et de ressources LT commerciaux et non commerciaux dans toutes les langues européennes.",
      p2a: "N'hésitez pas à visiter notre site internet.",
      p3: "La création de ce système de traduction a été motivée par la récente invasion de l'Ukraine par la Fédération de Russie. En tant que projet européen, ELG se tient aux côtés de l'Ukraine et veut aider tous les Ukrainiens arrivant en Europe ainsi que les personnes qui les accueillent dans leur communication.",
      p4: "Veuillez nous faire savoir si vous avez des questions ou rencontrez des problèmes avec le service via contact@european-language-grid.eu.",
    },
    languages: {
      English: "anglais",
      Ukrainian: "ukrainien",
      Russian: "russe",
      French: "français",
      Polish: "polonais",
    },
  },
  uk: {
    clear: "Видалити",
    copy: "Копіювати",
    translate: "Перекласти",
    translation: "Переклад",
    enter: "Введіть текст для перекладу.",
    selectLang: "Оберіть мову для сторінки",
    selectSrc: "Оберіть вихідну мову",
    selectTgt: "Оберіть цільову мову",
    selectModel: "Оберіть модель для використання",
    currentModelInfo: "Європейська мовна сітка нараховує різні моделі перекладу для одних і тих же мов. Ви можете вибрати між усіма наявними моделями.",
    currentModel: "Актуальна модель: ",
    funding: "Європейська мовна сітка фінансована програмою досліджень та інновацій Європейського Союзу Horizon 2020 згідно грантової угоди № 825627 (ELG)",
    information: "Інформація",
    informationTexts: {
        p1: "Це відкритий перекладач Європейської мовної сітки (ELG), що працює на основі служб машинного перекладу з каталогу ELG, наданого спільнотою ELG.",
        p2: "European Language Grid  — це платформа для мовних технологій (LT) в Європі, яка надає доступ до тисяч комерційних і некомерційних послуг і ресурсів LT усіма європейськими мовами.",
        p2a: "Не вагайтеся відвідувати наш веб-сайт.",
        p3: "Створення цієї системи перекладу було мотивоване нещодавнім вторгненням Російської Федерації в Україну. Як європейський проект, ELG підтримує Україну і хоче допомогти  у спілкуванні всім українцям, які прибувають до Європи, а також людям, які їх приймають.",
        p4: "Будь ласка, повідомте нам, якщо у вас виникли запитання або виникли проблеми із сервісом, за адресою contact@european-language-grid.eu.",
    },
    languages: {
        English: "англійська",
        Ukrainian: "українська",
        Russian: "російська",
        French: "французька",
        Polish: "польська",
    },
  },
  ru: {
    clear: "Очистить",
    copy: "Копировать",
    translate: "Перевести",
    translation: "Перевод",
    enter: "Введите текст для перевода.",
    selectLang: "Выберите язык для страницы",
    selectSrc: "Выберите исходный язык",
    selectTgt: "Выберите целевой язык",
    selectModel: "Выберите модель для использования",
    currentModelInfo: "Европейская языковая сетка учитывает различные модели перевода для одних и тех же языков. Вы можете выбрать между всеми доступными моделями.",
    currentModel: "Текущая модель: ",
    funding: "Европейская языковая сеть была профинансирована  исследовательской и инновационной программы Horizon 2020 Европейского Союза в рамках соглашения о гранте № 825627 (ELG).",
    information: "Информация",
    informationTexts: {
        p1: "Это открытый переводчик European Language Grid (ELG), основанный на услугах машинного перевода из каталога ELG, предоставленного сообществом ELG.",
        p2: "European Language Grid — это платформа для языковых технологий (LT) в Европе, предоставляющая доступ к тысячам коммерческих и некоммерческих LT-сервисов и ресурсов на всех европейских языках.",
        p2a: "Не стесняйтесь посещать наш веб-сайт.",
        p3: "Создание этой системы перевода было мотивировано недавним вторжением Российской Федерации в Украину. Как европейский проект, ELG поддерживает Украину и хочет помочь в общении всем украинцам, прибывающим в Европу, а также людям, принимающим их.",
        p4: "Пожалуйста, сообщите нам, если у вас возникнут вопросы или проблемы с сервисом, по адресу contact@european-language-grid.eu.",
    },
    languages: {
        English: "английский",
        Ukrainian: "украинский",
        Russian: "русский",
        French: "французский",
        Polish: "польский",
    },
  },
  pl: {
    clear: "Jasny",
    copy: "Kopiuj",
    translate: "Tłumaczyć",
    translation: "Tłumaczenie",
    enter: "Wpisz tekst do przetłumaczenia.",
    selectLang: "Wybierz język strony",
    selectSrc: "Wybierz język źródłowy",
    selectTgt: "Wybierz język docelowy",
    selectModel: "Wybierz model do użycia",
    currentModelInfo: "European Language Grid zlicza różne modele tłumaczeń dla tych samych języków. Możesz wybierać spośród wszystkich dostępnych modeli.",
    currentModel: "Aktualny model: ",
    funding:
      "European Language Grid otrzymał dofinansowanie z unijnego programu badawczego i innowacyjnego Horyzont 2020 w ramach umowy o grant nr 825627 (ELG)",
    information: "Informacja",
    informationTexts: {
      p1: "Jest to otwarty tłumacz European Language Grid (ELG), korzystający z usług tłumaczenia maszynowego z katalogu ELG, udostępnionego przez społeczność ELG.",
      p2: "European Language Grid to platforma technologii językowych (LT) w Europie, zapewniająca dostęp do tysięcy komercyjnych i niekomercyjnych usług i zasobów LT we wszystkich językach europejskich.",
      p2a: "Zapraszamy do odwiedzenia naszej strony internetowej.",
      p3: "Stworzenie tego systemu tłumaczeń było motywowane niedawną inwazją Federacji Rosyjskiej na Ukrainę. Jako projekt europejski, ELG wspiera Ukrainę i chce pomóc wszystkim Ukraińcom przybywającym do Europy, a także ludziom witającym ich w komunikacji.",
      p4: "W przypadku pytań lub problemów z usługą prosimy o kontakt pod adresem contact@european-language-grid.eu.",
    },
    languages: {
      English: "język angielski",
      Ukrainian: "ukraiński",
      Russian: "rosyjski",
      French: "francuski",
      Polish: "polski",
    },
  },
};

function getWindowWidth() {
  const { innerWidth: width} = window;
  return width;
}

function Home({languages, server}) {
  const router = useRouter();
  const {locale, locales, defaultLocale, asPath} = useRouter();
  const {setTheme} = useNextTheme();
  const {isDark, type} = useTheme();

  const {copy} = useClipboard();

  const [windowWidth, setWindowWidth] = useState();

  const infoModal = useModal();
  const pageLangModal = useModal();
  const srcLangModal = useModal();
  const tgtLangModal = useModal();
  const modelModal = useModal();

  const [srcLang, setSrcLang] = useState(languages.src[0]);
  const [tgtLang, setTgtLang] = useState(srcLang.tgt[0]);
  const [model, setModel] = useState(tgtLang.models[0]);

  const content = contents[locale];

  const defaultTranslation = content.translation;
  const [waiting, setWaiting] = useState(false);
  const {
    value: translation,
    setValue: setTranslation,
    reset: resetTranslation,
    bindings: bindingsTranslation,
  } = useInput(defaultTranslation);
  const {
    value: textToTranslate,
    setValue: setTextToTranslate,
    reset: resetTextToTranslate,
    bindings: bindingsTextToTranslate,
  } = useInput("");

  const StyledButton = styled(Button, {
    variants: {
      color: {
        orangeVivid: {
          background: "$orangeVivid",
          color: "$orangeDim",
        },
      },
    },
  });

  useEffect(() => {
    function handleResize() {
      setWindowWidth(getWindowWidth());
    }
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (
    <>
      <Head>
        <title>ELG Translate</title>
        <meta
          name="description"
          content="Machine translation system powered by ELG MT services."
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container
        as="main"
        display="flex"
        direction="column"
        alignItems="center"
        style={{minHeight: "100vh"}}
        gap={0}
      >
        <Row id="navbar" align="center">
          <Spacer x={0.5} />
          <Image
            src="/elg_flag_monogram.svg"
            alt="ELG Logo"
            width={89}
            height={36}
          />
          <StyledButton
            auto
            light
            icon={<InfoEmpty height={28} width={28} />}
            onClick={() => infoModal.setVisible(true)}
            style={{height: "48px", marginLeft: "auto"}}
            aria-label="information"
            aria-labelledby="information"
            aria-describedby="information about the page"
          />
          <Modal
            fullScreen
            closeButton
            autoMargin
            animated={false}
            aria-label="information"
            aria-labelledby="information"
            aria-describedby="information about the page"
            {...infoModal.bindings}
          >
            <Modal.Header>
              <Text h1>{content.information}</Text>
            </Modal.Header>
            <Modal.Body style={{alignItems: "center"}}>
              <Container
                gap={0}
                display="flex"
                direction="column"
                style={{maxWidth: "600px"}}
              >
                <Text margin="0">{content.informationTexts.p1}</Text>
                <Spacer y={0.5} />
                <Text margin="0">
                  {content.informationTexts.p2}{" "}
                  <Link href="https://live.european-language-grid.eu">
                    {content.informationTexts.p2a}
                  </Link>
                </Text>
                <Spacer y={0.5} />
                <Image
                  src="/eu_with_uk.png"
                  alt="EU stands with Ukraine"
                  width={500}
                  height={100}
                />
                <Spacer y={0.5} />
                <Text margin="0">{content.informationTexts.p3}</Text>
                <Spacer y={0.5} />
                <Text margin="0">{content.informationTexts.p4}</Text>
              </Container>
            </Modal.Body>
          </Modal>
          <Switch
            checked={isDark}
            onChange={(e) => setTheme(e.target.checked ? "dark" : "light")}
            iconOn={<SunLight fill="currentColor" />}
            iconOff={<HalfMoon fill="currentColor" />}
            aria-label="toggle the dark theme"
            aria-labelledby="theme switcher"
            aria-describedby="switch the color theme of the page"
          />
          <StyledButton
            auto
            light
            icon={<Language height={28} width={28} />}
            onClick={() => pageLangModal.setVisible(true)}
            style={{height: "48px"}}
            aria-label="page language"
            aria-labelledby="page language"
            aria-describedby="select the language of the page"
          />
          <Modal
            fullScreen
            closeButton
            animated={false}
            aria-label="page language"
            aria-labelledby="page language"
            aria-describedby="select the language of the page"
            {...pageLangModal.bindings}
          >
            <Modal.Header>
              <Text h1>{content.selectLang}</Text>
            </Modal.Header>
            <Modal.Body>
              <Container
                gap={0}
                display="flex"
                wrap="wrap"
                style={{maxWidth: "600px", gap: "1rem"}}
              >
                {locales.map(function (lang, i) {
                  return (
                    <StyledButton
                      key={i}
                      size="lg"
                      color="orangeVivid"
                      auto
                      onClick={() => {
                        pageLangModal.setVisible(false);
                        router.push(asPath, asPath, {locale: lang});
                      }}
                    >
                      {lang}
                    </StyledButton>
                  );
                })}
              </Container>
            </Modal.Body>
          </Modal>
          <Spacer x={0.5} />
        </Row>
        <Row>
          <StyledButton
            auto
            onClick={() => srcLangModal.setVisible(true)}
            style={{width: "calc(50% - 61px)"}}
            color="orangeVivid"
            aria-label="source languages"
            aria-labelledby="source languages"
            aria-describedby="select the source language to translate from"
          >
            {content.languages[srcLang.longName]}
          </StyledButton>
          <Modal
            fullScreen
            closeButton
            animated={false}
            aria-label="source languages"
            aria-labelledby="source languages"
            aria-describedby="select the source language to translate from"
            {...srcLangModal.bindings}
          >
            <Modal.Header>
              <Text h1>{content.selectSrc}</Text>
            </Modal.Header>
            <Modal.Body>
              <Container
                gap={0}
                display="flex"
                wrap="wrap"
                style={{maxWidth: "600px", gap: "1rem"}}
              >
                {languages.src.map(function (lang, i) {
                  return (
                    <StyledButton
                      key={i}
                      size="lg"
                      color="orangeVivid"
                      auto
                      onClick={() => {
                        srcLangModal.setVisible(false);
                        setSrcLang(lang);
                        var found = false;
                        lang.tgt.forEach((tgtCandidate, i) => {
                          if (tgtCandidate.shortName === tgtLang.shortName) {
                            setTgtLang(tgtCandidate);
                            setModel(tgtCandidate.models[0]);
                            found = true;
                          }
                        });
                        if (found === false) {
                          setTgtLang(lang.tgt[0]);
                          setModel(lang.tgt[0].models[0]);
                        }
                      }}
                    >
                      {content.languages[lang.longName]}
                    </StyledButton>
                  );
                })}
              </Container>
            </Modal.Body>
          </Modal>
          <StyledButton
            auto
            color="orangeVivid"
            icon={
              <DataTransferBoth
                style={{transform: "scaleX(-1) rotate(90deg)"}}
              />
            }
            aria-label="switch languages"
            aria-labelledby="switch languages"
            aria-describedby="exchange the source language with the target language"
            onClick={() => {
              var srcFound = false;
              languages.src.forEach((lang, i) => {
                if (
                  lang.shortName === tgtLang.shortName &&
                  srcFound === false
                ) {
                  setSrcLang(lang);
                  var found = false;
                  lang.tgt.forEach((tgtCandidate, i) => {
                    if (tgtCandidate.shortName === srcLang.shortName) {
                      setTgtLang(tgtCandidate);
                      setModel(tgtCandidate.models[0]);
                      found = true;
                    }
                  });
                  if (found === false) {
                    setTgtLang(lang.tgt[0]);
                    setModel(lang.tgt[0].models[0]);
                  }
                  srcFound = true;
                }
              });
              if (srcFound === false) {
                setSrcLang(languages.src[0]);
                setTgtLang(languages.src[0].tgt[0]);
                setModel(languages.src[0].tgt[0].models[0]);
              }
            }}
          />
          <StyledButton
            auto
            color="orangeVivid"
            onClick={() => tgtLangModal.setVisible(true)}
            style={{width: "calc(50% - 61px)"}}
            aria-label="target languages"
            aria-labelledby="target languages"
            aria-describedby="select the target language to translate to"
          >
            {content.languages[tgtLang.longName]}
          </StyledButton>
          <Modal
            fullScreen
            closeButton
            animated={false}
            aria-label="target languages"
            aria-labelledby="target languages"
            aria-describedby="select the target language to translate to"
            {...tgtLangModal.bindings}
          >
            <Modal.Header>
              <Text h1>{content.selectTgt}</Text>
            </Modal.Header>
            <Modal.Body>
              <Container
                gap={0}
                display="flex"
                wrap="wrap"
                style={{maxWith: "600px", gap: "1rem"}}
              >
                {srcLang.tgt.map(function (lang, i) {
                  return (
                    <StyledButton
                      key={i}
                      size="lg"
                      color="orangeVivid"
                      auto
                      onClick={() => {
                        tgtLangModal.setVisible(false);
                        setTgtLang(lang);
                        setModel(lang.models[0]);
                      }}
                    >
                      {content.languages[lang.longName]}
                    </StyledButton>
                  );
                })}
              </Container>
            </Modal.Body>
          </Modal>
          <StyledButton
            color="orangeVivid"
            auto
            onClick={() => modelModal.setVisible(true)}
            icon={<Svg3DSelectFace />}
            aria-label="models"
            aria-labelledby="models"
            aria-describedby="select the model to use"
          />
          <Modal
            fullScreen
            closeButton
            animated={false}
            aria-label="models"
            aria-labelledby="models"
            aria-describedby="select the model to use"
            {...modelModal.bindings}
          >
            <Modal.Header style={{flexDirection: "column"}}>
              <Text h1>{content.selectModel}</Text>
              <Text>
                {content.currentModelInfo}
              </Text>
              <Text>
                {content.currentModel}
                {model}
              </Text>
            </Modal.Header>
            <Modal.Body>
              <Container
                gap={0}
                display="flex"
                wrap="wrap"
                style={{maxWidth: "600px", gap: "1rem"}}
              >
                {tgtLang.models.map(function (modelCandidate, i) {
                  return (
                    <StyledButton
                      color="orangeVivid"
                      key={i}
                      size="lg"
                      auto
                      onClick={() => {
                        modelModal.setVisible(false);
                        setModel(modelCandidate);
                      }}
                    >
                      {modelCandidate}
                    </StyledButton>
                  );
                })}
              </Container>
            </Modal.Body>
          </Modal>
        </Row>
        <Container gap={0} display="flex" id="translationArea">
          <Card
            shadow={false}
            animated={false}
            style={{backgroundColor: "var(--nextui-colors-accents1)"}}
          >
            <Textarea
              {...bindingsTextToTranslate}
              shadow={false}
              animated={false}
              minRows={5}
              maxRows={10}
              placeholder={content.enter}
              fullWidth={true}
              onKeyPress={(event) => {
                if (event.key == 'Enter' && event.nativeEvent.ctrlKey == true) {
                  setWaiting(true);
                  translate(model, textToTranslate, server).then((res) => {
                    setTranslation(res);
                    setWaiting(false);
                  });
                }
              }}
            />
            <Container
              display="flex"
              justify="right"
              alignItems="center"
              gap={0}
            >
              <StyledButton
                size="sm"
                auto
                color="orangeVivid"
                onClick={() => resetTextToTranslate()}
                aria-label="clear"
                aria-labelledby="clear"
                aria-describedby="clear the text to translate"
              >
                {content.clear}
              </StyledButton>
              <Spacer x={0.5} />
              <Tooltip content={"Copied"} trigger="click" rounded>
                <StyledButton
                  size="sm"
                  auto
                  color="orangeVivid"
                  onClick={() => {
                    copy(textToTranslate);
                  }}
                  aria-label="copy"
                  aria-labelledby="copy"
                  aria-describedby="copy the text to translate"
                >
                  {content.copy}
                </StyledButton>
              </Tooltip>
            </Container>
          </Card>
          <Card
            shadow={false}
            animated={false}
            css={{backgroundColor: "$primaryLight"}}
          >
            <Textarea
              {...bindingsTranslation}
              readOnly
              shadow={false}
              animated={false}
              minRows={5}
              maxRows={10}
              fullWidth={true}
              status="primary"
            />
            <Container
              display="flex"
              justify="right"
              alignItems="center"
              gap={0}
            >
              <Tooltip content={"Copied"} trigger="click" rounded>
                <StyledButton
                  size="sm"
                  auto
                  color="orangeVivid"
                  onClick={() => {
                    copy(translation);
                  }}
                  aria-label="copy"
                  aria-labelledby="copy"
                  aria-describedby="copy the translated text"
                >
                  {content.copy}
                </StyledButton>
              </Tooltip>
            </Container>
          </Card>
        </Container>
        {waiting === true ? (
          <StyledButton
            clickable={false}
            color="orangeVivid"
            style={{width: "100%"}}
          >
            <Loading color="secondary" type="points-opacity" />
          </StyledButton>
        ) : (
          <StyledButton
            color="orangeVivid"
            onClick={() => {
              setWaiting(true);
              translate(model, textToTranslate, server).then((res) => {
                setTranslation(res);
                setWaiting(false);
              });
            }}
            style={{width: "100%"}}
            aria-label="translate"
            aria-labelledby="translate"
            aria-describedby="translate the text"
          >
            {content.translate}
            {windowWidth >= 960 && " (Ctrl+Enter)"}
          </StyledButton>
        )}
        <Spacer y={0.5} />
        <Row
          id="footer"
          align="center"
          style={{
            marginTop: "auto",
          }}
        >
          <Spacer x={0.5} />
          <Image
            src="/eu_flag.svg"
            alt="European Union flag"
            width={48}
            height={32}
          />
          <Spacer x={0.5} />
          <Text
            size={12}
            margin="0"
            style={{width: "calc(100% - 48px - 4rem)"}}
          >
            {content.funding}
          </Text>
          <Spacer x={0.5} />
        </Row>
        <Spacer y={0.5} />
      </Container>
    </>
  );
}

export async function getServerSideProps() {
  const res = await fetch(`${process.env.SERVER}/api/languages`);
  const languages = await res.json();
  return {
    props: {
      languages,
      server: process.env.DOMAIN,
    },
  };
}

async function translate(model, content, server) {
  try {
    const res = await fetch(`${server}/api/process/${model}`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({content: content}),
    });
    const {response} = await res.json();
    return response;
  } catch (error) {
    return "Error during the translation, please try again.";
  }
}

export default Home;
