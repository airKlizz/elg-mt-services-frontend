export default async function handler(req, res) {
  const resp = await fetch(`${process.env.API}/languages`);
  const response = await resp.json();
  res.status(200).json(response);
}
