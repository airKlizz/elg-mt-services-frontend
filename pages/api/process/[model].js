export default async function handler(req, res) {
  if (req.method === "POST") {
    console.log("process - ", req.headers);
    const {model} = req.query;
    const resp = await fetch(`${process.env.API}/process/${model}`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({content: req.body.content}),
    });
    if (resp.status === 200) {
      const {response} = await resp.json();
      res.status(200).json({response: response});
    } else if (resp.status === 401 || resp.status === 403) {
      const {detail} = await resp.json();
      res.status(200).json({response: detail});
    } else if (resp.status === 404) {
      const {detail} = await resp.json();
      res.status(404).json({response: `${detail}. Please try again.`});
    } else {
      res
        .status(404)
        .json({response: "Issue during the service call. Please try again."});
    }
  } else {
    res.status(405).send({message: "Only POST requests allowed"});
  }
}
