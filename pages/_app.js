import "../styles/globals.css";

import { NextUIProvider, createTheme } from "@nextui-org/react";

import { ThemeProvider as NextThemesProvider } from "next-themes";

const lightTheme = createTheme({
  type: "light",
  theme: {
    colors: {
      orangeDim: "#523d14",
      orangeDark: "#a37a29",
      orange: "#d6ad5c",
      orangeVivid: "#ebb447",
      orangeLight: "#ebd6ad",

      primaryLight: "$orangeLight",
      primary: "$orangeDim",
      primaryDark: "$orangeDim",
      primaryShadow: "$orangeLight",
      secondary: "$orangeDim",
    },
    breakpoints: {
      xs: "960px",
      lg: "1280px",
      xl: "1280px",
    },
    radii: {
      xs: "7px",
      sm: "9px",
      md: "0",
      base: "0",
      lg: "0",
      xl: "0",
      squared: "33%",
      rounded: "50%",
      pill: "9999px",
    },
  },
});

const darkTheme = createTheme({
  type: "dark",
  theme: {
    colors: {
      orangeDim: "#523d14",
      orangeDark: "#a37a29",
      orange: "#d6ad5c",
      orangeVivid: "#ebb447",
      orangeLight: "#ebd6ad",

      primaryLight: "$orangeDim",
      primary: "$orangeVivid",
      primaryDark: "$orangeVivid",
      primaryShadow: "$orangeDim",
      secondary: "$orangeDim",
    },
    breakpoints: {
      xs: "960px",
      lg: "1280px",
      xl: "1280px",
    },
    radii: {
      xs: "7px",
      sm: "9px",
      md: "0",
      base: "0",
      lg: "0",
      xl: "0",
      squared: "33%",
      rounded: "50%",
      pill: "9999px",
    },
  },
});

function MyApp({ Component, pageProps }) {
  return (
    <NextThemesProvider
      defaultTheme="system"
      attribute="class"
      value={{
        light: lightTheme.className,
        dark: darkTheme.className,
      }}
    >
      <NextUIProvider>
        <Component {...pageProps} />
      </NextUIProvider>
    </NextThemesProvider>
  );
}

export default MyApp;
