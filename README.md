# ELG translation system UI

This UI has been created to work with the [ELG MT Proxy](https://gitlab.com/airKlizz/elg-proxy-uk-mt).
It is built using Next.js and NextUI.

To test is locally, you need to run the ELG MT Proxy server and set up the environnement varibles:

```bash
git clone https://gitlab.com/airKlizz/elg-mt-services-frontend
cd elg-mt-services-frontend

cp .env.development .env.local

docker run --name redis-docker -p 6379:6379 -d redis
docker run --name elg-proxy-uk-mt -p 8000:8000 -d registry.gitlab.com/airklizz/elg-proxy-uk-mt

npm install
npm run dev
```

The UI should be accessible at http://localhost:3000.

## Screenshots

![mobile_light](screenshots/mobile_light.png)
![mobile_dark](screenshots/mobile_dark.png)
![desktop_light](screenshots/desktop_light.png)
![desktop_dark](screenshots/desktop_dark.png)

# Next.js README

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
